import * as L from 'leaflet';
import * as $ from 'jquery';

import {spinkit} from './spinkit.js';

export const extMap = {
  mixins:[spinkit],
  data() {
    return {
      map: undefined,
      $map: undefined,
      position: undefined
    };
  },
  methods: {
    setMaps(properties) {
      this.map = properties.map;
      this.$map = properties.$map;
    },
    getPosition() {
      this.addLoader(this.$map);

      var resolve = function(position) {
        this.removeLoader(this.$map);
        return this.position = position;
      };
      var reject = function() {
        this.removeLoader(this.$map);
        console.error("extmap:getPosition:navigator.geolocation.getCurrentPosition(error to get user position).");
      };

      var promise = new Promise(function(resolve, reject) {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(resolve, reject);
        } else {
          console.error("Geolocation is not supported by this browser.");
        }
      });

      return promise;
    },
    getGeoJson(path) {

      let $loader = this.addLoader(this.$map);

      let resolve = function(geoJson) {
        this.removeLoader(this.$map, $loader);
        return geoJSON;
      };
      let reject = function() {
        this.removeLoader(this.$map, $loader);
        console.error("extmap:setGeoJsonLayer(error to get data).");
      };

      let promise = new Promise(function(resolve, reject) {
        $.getJSON(path,
          resolve,
          reject);
      });

      return promise;
    }
  }
};
