## ENV configuration
create a file .env.[environment_name] with the following environments variables
with the configuration depending of the environment.

#### Add Environment variable description
```
NODE_ENV: node environment value
SERVER_URL : server url
SERVER_PORT : server port
GEO_INFO_PARAM : parameter to load geo json on the map
GEO_INFO_URL : url to load the map with geojson information
API_PATH : Api url
```
