# GeoRisk

The application GeoRisk provide the statistic of the different sinister that took place on a geo-localization.
It allow to add different layer of event to see the problematics on specific place.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## ENV file

Create your own environment file to declare your local variable.

[Documentation](https://github.com/vincentBaylly/geoRisk/blob/master/documentation/ENV.md)

## License
[Apache 2.0 License](https://img.shields.io/crates/l/:crate.svg?style=flat)
[Community License](https://github.com/vincentBaylly/communityMapper/blob/master/LICENSE)
