import * as $ from 'jquery';

export const spinkit = {
  methods: {
    addLoader($container) {
      if ($container.find('.sk-folding-cube').length !== 0) {
        return;
      }

      $container.append(
        '<div class="sk-folding-cube">' +
        '<div class="sk-cube1 sk-cube"></div>' +
        '<div class="sk-cube2 sk-cube"></div>' +
        '<div class="sk-cube4 sk-cube"></div>' +
        '<div class="sk-cube3 sk-cube"></div>' +
        '</div>');

      let $loader = $container.find('.sk-folding-cube');

      return $loader;
    },
    removeLoader($container) {
      let $loader = $container.find('.sk-folding-cube');
      if ($loader.length === 0) {
        return;
      }

      $loader.remove();
    }
  }
};
