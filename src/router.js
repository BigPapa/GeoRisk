import Vue from "vue";
import Router from "vue-router";
import Home from "./components/LandingPage.vue";
import SignIn from "./components/SignIn.vue";
import ResetPassword from "./components/ResetPassword.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/signin",
      name: "signin",
      component: SignIn
    },
    {
      path: "/resetpassword/:link",
      name: "resetpassword",
      component: ResetPassword
    }
  ]
});
